import React from 'react'
import Button from '@mui/material/Button';

const App = () => {
  return (
    <Button>Hello World!</Button>
  )
}

export default App;