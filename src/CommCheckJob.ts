import { ImageAnnotatorClient } from '@google-cloud/vision';
import axios from 'axios';
enum CommCheckStatus {
    PENDING = 'PENDING',
    CHECKING = 'CHECKING',
    NOPOINTS = 'NO_POINTS',
    SOMEMATCHES = 'SOME_MATCHES',
    NOTCHECKED = 'NOT_CHECKED',
    COPYVIO = 'COPY_VIOLATION',
    FAILED = 'FAILED',
}

export default class CommCheckJob {
    client: ImageAnnotatorClient;
    public status: CommCheckStatus;
    public points: number;
    public isDone: boolean = false;
    public insightURLs: string[] = [];

    constructor(public filename: string, public pageID: string, client: ImageAnnotatorClient) {
        this.status = CommCheckStatus.PENDING;
        this.points = -2;
        this.client = client;
    }

     // Get the point count for a given image (returns a number)
     async getPoints(filename: string, pageID: string) : Promise<number> {
        // Get the image URL from the filename
        const imageUrlQ = await axios.get(`https://commons.wikimedia.org/w/api.php?action=query&titles=${encodeURI(filename)}&prop=imageinfo&iiprop=url|user&format=json`);
        const imageUrl = imageUrlQ.data.query.pages[pageID].imageinfo[0].url;
        const uploaduser = imageUrlQ.data.query.pages[pageID].imageinfo[0].user;

        // Get the edit count for the uploader
        const editCountQ = await axios.get(`https://commons.wikimedia.org/w/api.php?action=query&list=users&ususers=${encodeURI(uploaduser)}&usprop=editcount&format=json`);
        const editCount = editCountQ.data.query.users[0].editcount;

        if (editCount > 200) return -1; // If the user has more than 200 edits, we don't want to check them

        // Detect similar images on the web to a local file
        const [result] = await this.client.webDetection(imageUrl);
        const webDetection = result.webDetection;

        // Start image point counter
        let imagePoints = 0;

        if (webDetection.fullMatchingImages.length) {
            imagePoints += webDetection.fullMatchingImages.length * 2;
            this.insightURLs = webDetection.fullMatchingImages.map(image => image.url);
        }
        

        if (webDetection.partialMatchingImages.length) {
            imagePoints += webDetection.partialMatchingImages.length;
            this.insightURLs = this.insightURLs.concat(webDetection.partialMatchingImages.map(image => image.url));
        } 
        
        return imagePoints;
    }

    public async run() {
        try {
            this.status = CommCheckStatus.CHECKING;
            this.points = await this.getPoints(this.filename, this.pageID);
            if (this.points > 5) {
                this.status = CommCheckStatus.COPYVIO;
            } else if (this.points > 0) {
                this.status = CommCheckStatus.SOMEMATCHES;
            } else if (this.points == -1) {
                this.status = CommCheckStatus.NOTCHECKED;
            } else {
                this.status = CommCheckStatus.NOPOINTS;
            }
            this.isDone = true;
            return this.status;
        } catch (error) {
            console.error(error);
            this.status = CommCheckStatus.FAILED;
            this.isDone = true;
        }
    }
}