import vision from '@google-cloud/vision';
import CommCheck from './CommCheck';


// Creates a client
const client = new vision.ImageAnnotatorClient();

// Run
const commCheck = new CommCheck(client);
commCheck.run();