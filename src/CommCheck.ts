import { ImageAnnotatorClient } from "@google-cloud/vision";
import axios from 'axios';
import CommCheckJob from "./CommCheckJob";

export default class CommCheck {
    client: ImageAnnotatorClient;
    checkedFiles: string[] = [];

    constructor(client: ImageAnnotatorClient) {
        this.client = client;
    }

    addToChecked(filename: string) {
        this.checkedFiles.push(filename);
        // Remove the last element if the array is longer than 100
        if (this.checkedFiles.length > 100) {
            this.checkedFiles.shift();
        }
    }



    async run() {
        // Make a get request to Wikimedia commons
        const jobs: CommCheckJob[] = [];
        
        setInterval(async () => {
            const apiR = await axios.get('https://commons.wikimedia.org/w/api.php?action=query&list=categorymembers&cmtitle=Category:CC-BY-SA-4.0&cmsort=timestamp&cmdir=desc&cmlimit=50&format=json');
            const recentImages = apiR.data.query.categorymembers;
            for (const image of recentImages) {
                if (this.checkedFiles.includes(image.title)) continue; // Skip if the image has already been checked
                this.addToChecked(image.title);
                jobs.push(new CommCheckJob(image.title, image.pageid, this.client));
            }
            
            // Run the jobs
            const promiseArr = jobs.map(job => job.run().then(()=>{
                console.log(`${job.filename}: ${job.status}`);
                // Remove the job from the array
                jobs.splice(jobs.indexOf(job), 1);
            }));
            await Promise.all(promiseArr);
        }, 7500);
    }
}