# CommCheck

A bot that uses Google Cloud Vision to automatically check for copyright violations on Wikimedia commons

## Getting started

You will need a Google Cloud account and a Google Cloud Vision API key. Follow the instructions at https://cloud.google.com/vision/docs/quickstart-client-libraries to get started. Once you have your keys, save them in this root directory as keys.json.

You also need node.js and npm.

To build, run `tsc` in the root directory. To run, run `node build/main`.